package domain;

public record Player(String alias, int scorePerMinute, int wins, int loses) {
    public Player overWriteStats(int scorePerMinute, int wins, int loses){
        return new Player(alias(),scorePerMinute, wins, loses);
    }
}
